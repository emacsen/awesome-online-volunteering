---
name: The Resolution Project
website: https://resolutionproject.org
volunteer_page: https://resolutionproject.org/become-a-guide/
categories: Entrepreneurship
---

The Resolution Project works with business Mentors and Guides to help mentor Fellows to launch ventures that help support themselves and their communities.
