---
name: CEDR Digital Corps
website: https://cedrdigitalcorps.org
volunteer_page: https://cedrdigitalcorps.org/volunteer/
categories: Disaster
---

CEDR Digital Corps is a group of volunteers who help during disasters by using technology and social media to collect and share important information. This helps first responders and people in affected areas stay safe and get the help they need.
