---
name: The National Map Corps
website: https://www.usgs.gov/core-science-systems/ngp/tnm-corps
license: Public Domain
categories: Mapping
---

The National Map Corps (TNS Corps) asks volunteers to collect or correct information on structures (buildings, bridges, etc.) in TheNational Map database.
