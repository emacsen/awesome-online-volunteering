---
name: JewishGen
website: https://www.jewishgen.org/
volunteer_page: https://www.jewishgen.org/databases/Cemetery/JOWBR_FAQ.htm#q4
categories: history
---
The JewishGen database, from the Museum of Jewish Heritage, is a searchable database of Jewish genealogical records. You can collect records and submit them to the database, increasing knowledge about Jewish families around the world.
