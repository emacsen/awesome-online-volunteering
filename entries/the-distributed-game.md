---
name: The Distributed Game
website: https://wikidata-game.toolforge.org/distributed
categories: Mapping, History, Wikimedia
organization_type: 501c3
license: CC0
---

Wikidata provides structured data to the Wikipedia project on nearly every topic. The Distributed Game looks for inconsistencies in the data and provides a fun way to help correct information in Wikidata, which feeds into both it and Wikipedia.

