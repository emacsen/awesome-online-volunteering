---
name: Mozilla
website: https://mozilla.org
volunteer_page: https://www.mozilla.org/en-US/contribute/
license: Mozilla Public License
categories: Translation
organization_type: 501c3
---

Do you use the popular Mozilla Firefox browser, or one of the other Mozilla programs? You can contribute to the Mozilla in a variety of ways, from contributing directly to the source code, translatingprogram elements, documentation or generally helping with their online knowledge base!
