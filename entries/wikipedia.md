---
name: Wikipedia
website: https://www.wikipedia.org
organization_type: 501c3
license: CC BY-SA 3.0
pinned: Wikimedia
categories: Wikimedia
---

Wikipedia is a free online encyclopedia written and edited by thousands of worldwide volunteers. Contributors to Wikipedia, known as editors, come from all walks of life. In addition to contributing new articles, volunteers may contribute to existing articles, or translate articles from one language to another.
