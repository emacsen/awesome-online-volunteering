---
name: Paper Airplanes
website: https://www.paper-airplanes.org/
categories: Teaching, Language
organization_type: 501c3
---

Paper Airplanes volunteers provide English language lessons and speaking practice to students from conflict-affected countries, such as Syria and Ukraine. There are also opportunities to mentor new coders studying in the Women in Tech program. For the English program, the commitment is 2+ hours per week for an 18-week semester, using the curriculum provided.

