---
name: NLW Crowd - Welsh Heritage Crowdsourcing Projects
website: https://torf.llyfrgell.cymru/s/llgc/page/Home
categories: history, photos, mapping, transcription
---
The NLW Crowd - Welsh Heritage Crowdsourcing Projects from the National Library of Wales is a platform allowing citizens to help capture and preserve Welsh culture in a variety of ways, including tagging photos, transcription or identifying locations in historical maps.
