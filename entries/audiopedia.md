---
name: Audiopedia
website: https://audiopedia.org
volunteer_page: https://www.audiopedia.org/volunteer
categories: Audio, Women
organization_type: German Non-Profit
license: N/A
---

Audiopedia is a resource containing answers to many questions for illiterate women around the world in audio/spoken form.

