This project consists of both source code and collection of files, ie "entries".

The entries are licensed under [Creative Commons Attribution 4.0 International License](CC-BY-4.0-International.txt).

The assets `iconmoon.css` and the `fonts/` directory are from [Font Awesome](https://fontawesome.com/)

The remainder of the project is available under the [Apache 2.0 License](APACHE-LICENSE-2_0.txt).
